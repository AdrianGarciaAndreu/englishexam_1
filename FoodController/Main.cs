﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace FoodController
{
    public partial class MainForm : Form
    {

        RootObject productList;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void HomreToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        private void LoadData(String fileName) {

            String dataPath = Application.StartupPath + "/data/";
            using (FileStream fs = File.Open(dataPath + fileName, FileMode.Open, FileAccess.Read))
            {

                using (StreamReader sr = new StreamReader(fs, Encoding.GetEncoding("ISO-8859-1")))
                {
                    String line;
                    StringBuilder sb = new StringBuilder();

                    while ((line = sr.ReadLine()) != null)
                    {
                        sb.AppendLine(line);
                    }

                    this.productList = new RootObject();
                    this.productList = JsonConvert.DeserializeObject<RootObject>(sb.ToString());

                }
            }
        }

        private void showLoadedData()
        {

            foreach (Product p in this.productList.products)
            {
                String productName = p.productName; String description = p.description;
                String qty = Convert.ToString(p.qty); String price = Convert.ToString(p.price);

                String result = productName + ", (x"+qty+"), "+price+" €";

                this.listBox_Products.Items.Add(result);
            }

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

            Password passForm = new Password();
            DialogResult result = passForm.ShowDialog(this);

            String passwordResult = result.ToString();
            if(passwordResult=="Cancel"){
                Application.Exit();
            }


            LoadData("products.json");
            showLoadedData();
        }
    }
}
