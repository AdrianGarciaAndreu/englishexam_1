﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoodController
{

    public partial class Password : Form
    {

        String correctPassword = "1111";
        int times = 1;

        public Password()
        {
            InitializeComponent();
        }

        private void Password_Load(object sender, EventArgs e)
        {
            this.AcceptButton = passBtn;
        }

        private void PassBtn_Click(object sender, EventArgs e)
        {

            String enteredPass = passTextBox.Text.ToString();

                if (enteredPass == correctPassword)
                {
                    this.DialogResult = DialogResult.OK;
                    this.Dispose();
                } else {
                    if (times==3) {
                        this.DialogResult = DialogResult.Cancel;
                        this.Dispose();
                    } else {
                        times++;
                        MessageBox.Show("Incorrect password, try again.");
                        passTextBox.Text = "";
                    }

                }   
        
        }
    }
}
