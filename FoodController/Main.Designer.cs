﻿namespace FoodController
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelListFood = new System.Windows.Forms.Label();
            this.listBox_Products = new System.Windows.Forms.ListBox();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.labelEditList = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataRawToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelListFood
            // 
            this.labelListFood.AutoSize = true;
            this.labelListFood.Location = new System.Drawing.Point(238, 63);
            this.labelListFood.Name = "labelListFood";
            this.labelListFood.Size = new System.Drawing.Size(161, 25);
            this.labelListFood.TabIndex = 0;
            this.labelListFood.Text = "List of Products";
            this.labelListFood.Click += new System.EventHandler(this.Label1_Click);
            // 
            // listBox_Products
            // 
            this.listBox_Products.FormattingEnabled = true;
            this.listBox_Products.ItemHeight = 25;
            this.listBox_Products.Location = new System.Drawing.Point(54, 109);
            this.listBox_Products.Name = "listBox_Products";
            this.listBox_Products.Size = new System.Drawing.Size(532, 479);
            this.listBox_Products.TabIndex = 1;
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(664, 409);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(189, 96);
            this.btn_delete.TabIndex = 3;
            this.btn_delete.Text = "Delete Product";
            this.btn_delete.UseVisualStyleBackColor = true;
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(664, 255);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(189, 96);
            this.btn_add.TabIndex = 4;
            this.btn_add.Text = "Add Product";
            this.btn_add.UseVisualStyleBackColor = true;
            // 
            // labelEditList
            // 
            this.labelEditList.AutoSize = true;
            this.labelEditList.Location = new System.Drawing.Point(709, 175);
            this.labelEditList.Name = "labelEditList";
            this.labelEditList.Size = new System.Drawing.Size(89, 25);
            this.labelEditList.TabIndex = 5;
            this.labelEditList.Text = "Edit List";
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homreToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(934, 40);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homreToolStripMenuItem
            // 
            this.homreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataListToolStripMenuItem,
            this.dataRawToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.homreToolStripMenuItem.Name = "homreToolStripMenuItem";
            this.homreToolStripMenuItem.Size = new System.Drawing.Size(98, 36);
            this.homreToolStripMenuItem.Text = "Menu";
            this.homreToolStripMenuItem.Click += new System.EventHandler(this.HomreToolStripMenuItem_Click);
            // 
            // dataListToolStripMenuItem
            // 
            this.dataListToolStripMenuItem.Name = "dataListToolStripMenuItem";
            this.dataListToolStripMenuItem.Size = new System.Drawing.Size(359, 44);
            this.dataListToolStripMenuItem.Text = "Main Form";
            // 
            // dataRawToolStripMenuItem
            // 
            this.dataRawToolStripMenuItem.Name = "dataRawToolStripMenuItem";
            this.dataRawToolStripMenuItem.Size = new System.Drawing.Size(359, 44);
            this.dataRawToolStripMenuItem.Text = "Data Viewer";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(359, 44);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(359, 44);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 624);
            this.Controls.Add(this.labelEditList);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.listBox_Products);
            this.Controls.Add(this.labelListFood);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Main form";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelListFood;
        private System.Windows.Forms.ListBox listBox_Products;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Label labelEditList;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem homreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataRawToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}

