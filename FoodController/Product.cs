﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace FoodController
{
    public class Product
    {
        public int id { get; set; }
        public string productName { get; set; }
        public string description { get; set; }
        public int qty { get; set; }
        public double price { get; set; }


    }

    public class RootObject
    {
        public List<Product> products { get; set; }
    }

}
