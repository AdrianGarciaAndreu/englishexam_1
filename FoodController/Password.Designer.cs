﻿namespace FoodController
{
    partial class Password
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.passTextBox = new System.Windows.Forms.TextBox();
            this.passLbl = new System.Windows.Forms.Label();
            this.passBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // passTextBox
            // 
            this.passTextBox.Location = new System.Drawing.Point(70, 63);
            this.passTextBox.Name = "passTextBox";
            this.passTextBox.Size = new System.Drawing.Size(464, 31);
            this.passTextBox.TabIndex = 0;
            // 
            // passLbl
            // 
            this.passLbl.AutoSize = true;
            this.passLbl.Location = new System.Drawing.Point(65, 24);
            this.passLbl.Name = "passLbl";
            this.passLbl.Size = new System.Drawing.Size(265, 25);
            this.passLbl.TabIndex = 1;
            this.passLbl.Text = "Enter password to access:";
            // 
            // passBtn
            // 
            this.passBtn.Location = new System.Drawing.Point(574, 43);
            this.passBtn.Name = "passBtn";
            this.passBtn.Size = new System.Drawing.Size(151, 70);
            this.passBtn.TabIndex = 2;
            this.passBtn.Text = "Enter";
            this.passBtn.UseVisualStyleBackColor = true;
            this.passBtn.Click += new System.EventHandler(this.PassBtn_Click);
            // 
            // Password
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 145);
            this.ControlBox = false;
            this.Controls.Add(this.passBtn);
            this.Controls.Add(this.passLbl);
            this.Controls.Add(this.passTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Password";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Password";
            this.Load += new System.EventHandler(this.Password_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox passTextBox;
        private System.Windows.Forms.Label passLbl;
        private System.Windows.Forms.Button passBtn;
    }
}