﻿namespace FoodController
{
    partial class DataViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.DataViewer_title_lbl = new System.Windows.Forms.Label();
            this.exportGroup = new System.Windows.Forms.GroupBox();
            this.radioXML = new System.Windows.Forms.RadioButton();
            this.radioTxT = new System.Windows.Forms.RadioButton();
            this.btn_export = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataRawToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportGroup.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(36, 167);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(700, 386);
            this.textBox1.TabIndex = 0;
            // 
            // DataViewer_title_lbl
            // 
            this.DataViewer_title_lbl.AutoSize = true;
            this.DataViewer_title_lbl.Location = new System.Drawing.Point(325, 105);
            this.DataViewer_title_lbl.Name = "DataViewer_title_lbl";
            this.DataViewer_title_lbl.Size = new System.Drawing.Size(102, 25);
            this.DataViewer_title_lbl.TabIndex = 1;
            this.DataViewer_title_lbl.Text = "Raw data";
            // 
            // exportGroup
            // 
            this.exportGroup.Controls.Add(this.radioTxT);
            this.exportGroup.Controls.Add(this.radioXML);
            this.exportGroup.Location = new System.Drawing.Point(36, 559);
            this.exportGroup.Name = "exportGroup";
            this.exportGroup.Size = new System.Drawing.Size(287, 84);
            this.exportGroup.TabIndex = 2;
            this.exportGroup.TabStop = false;
            this.exportGroup.Text = "Export formats";
            this.exportGroup.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // radioXML
            // 
            this.radioXML.AutoSize = true;
            this.radioXML.Location = new System.Drawing.Point(57, 39);
            this.radioXML.Name = "radioXML";
            this.radioXML.Size = new System.Drawing.Size(87, 29);
            this.radioXML.TabIndex = 0;
            this.radioXML.TabStop = true;
            this.radioXML.Text = "XML";
            this.radioXML.UseVisualStyleBackColor = true;
            this.radioXML.CheckedChanged += new System.EventHandler(this.RadioButton1_CheckedChanged);
            // 
            // radioTxT
            // 
            this.radioTxT.AutoSize = true;
            this.radioTxT.Location = new System.Drawing.Point(170, 39);
            this.radioTxT.Name = "radioTxT";
            this.radioTxT.Size = new System.Drawing.Size(83, 29);
            this.radioTxT.TabIndex = 1;
            this.radioTxT.TabStop = true;
            this.radioTxT.Text = "TXT";
            this.radioTxT.UseVisualStyleBackColor = true;
            // 
            // btn_export
            // 
            this.btn_export.Location = new System.Drawing.Point(511, 579);
            this.btn_export.Name = "btn_export";
            this.btn_export.Size = new System.Drawing.Size(225, 48);
            this.btn_export.TabIndex = 3;
            this.btn_export.Text = "Export Data";
            this.btn_export.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homreToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(770, 42);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homreToolStripMenuItem
            // 
            this.homreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataListToolStripMenuItem,
            this.dataRawToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.homreToolStripMenuItem.Name = "homreToolStripMenuItem";
            this.homreToolStripMenuItem.Size = new System.Drawing.Size(98, 38);
            this.homreToolStripMenuItem.Text = "Menu";
            // 
            // dataListToolStripMenuItem
            // 
            this.dataListToolStripMenuItem.Name = "dataListToolStripMenuItem";
            this.dataListToolStripMenuItem.Size = new System.Drawing.Size(277, 44);
            this.dataListToolStripMenuItem.Text = "Main Form";
            // 
            // dataRawToolStripMenuItem
            // 
            this.dataRawToolStripMenuItem.Name = "dataRawToolStripMenuItem";
            this.dataRawToolStripMenuItem.Size = new System.Drawing.Size(277, 44);
            this.dataRawToolStripMenuItem.Text = "Data Viewer";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(277, 44);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(277, 44);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // DataViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 675);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btn_export);
            this.Controls.Add(this.exportGroup);
            this.Controls.Add(this.DataViewer_title_lbl);
            this.Controls.Add(this.textBox1);
            this.Name = "DataViewer";
            this.Text = "Data Viewer";
            this.exportGroup.ResumeLayout(false);
            this.exportGroup.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label DataViewer_title_lbl;
        private System.Windows.Forms.GroupBox exportGroup;
        private System.Windows.Forms.RadioButton radioTxT;
        private System.Windows.Forms.RadioButton radioXML;
        private System.Windows.Forms.Button btn_export;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem homreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataRawToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
    }
}